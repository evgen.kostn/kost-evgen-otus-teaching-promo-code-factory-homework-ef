﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class PromoCode
        : BaseEntity
    {
        [Required]
        [StringLength(100)]
        public string Code { get; set; }

        [Required]
        public string ServiceInformation { get; set; }

        [Required]
        public DateTime BeginDate { get; set; }

        public DateTime EndDate { get; set; }
        public string PartnerName { get; set; }
        public Guid PartnerId { get; set; }
        public virtual Employee PartnerManager { get; set; }
        public string PreferenceName { get; set; }
        public Guid PreferenceId { get; set; }
        public virtual Preference Preference { get; set; }
    }
}