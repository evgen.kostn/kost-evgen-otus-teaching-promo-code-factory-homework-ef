﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class CustomerPreference
    {
        [Required]
        public Guid CustomerId { get; set; }

        public virtual Customer Customer { get; set; }

        [Required]
        public Guid PreferenceId { get; set; }

        public virtual Preference Preference { get; set; }
    }
}