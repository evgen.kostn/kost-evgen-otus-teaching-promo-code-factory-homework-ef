﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Customer
        : BaseEntity
    {
        [Required]
        [StringLength(100)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(100)]
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        public virtual List<Preference> Preferences { get; set; } = new List<Preference>();
        public virtual List<PromoCode> Promocodes { get; set; } = new List<PromoCode>();
        public virtual List<CustomerPreference> CustomerPreferences { get; set; } = new List<CustomerPreference>();
    }
}