﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Preference
        : BaseEntity
    {
        [Required]
        [StringLength(500)]
        public string Name { get; set; }

        public virtual List<Customer> Customers { get; set; } = new List<Customer>();
        public virtual List<PromoCode> Promocodes { get; set; } = new List<PromoCode>();
        public virtual List<CustomerPreference> CustomerPreferences { get; set; } = new List<CustomerPreference>();
    }
}