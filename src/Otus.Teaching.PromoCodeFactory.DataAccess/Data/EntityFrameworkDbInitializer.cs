﻿using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class EntityFrameworkDbInitializer
        : IDbInitializer
    {
        private readonly DatabaseContext _dataContext;

        public EntityFrameworkDbInitializer(DatabaseContext dataContext)
        {
            _dataContext = dataContext;
        }

        public void InitializeDb()
        {
            //_dataContext.Database.EnsureDeleted();
            _dataContext.Database.EnsureCreated();

            _dataContext.AddRange(FakeDataFactory.Promocodes);
            _dataContext.SaveChanges();

            _dataContext.AddRange(FakeDataFactory.Employees);
            _dataContext.SaveChanges();

            _dataContext.AddRange(FakeDataFactory.Customers);
            _dataContext.SaveChanges();

            _dataContext.AddRange(FakeDataFactory.Preferences);
            _dataContext.SaveChanges();
        }
    }
}