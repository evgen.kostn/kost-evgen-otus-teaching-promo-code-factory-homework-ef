﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class DatabaseContext : DbContext
    {
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Preference> Preferences { get; set; }
        public DbSet<CustomerPreference> CustomerPreferences { get; set; }
        public DbSet<PromoCode> Promocodes { get; set; }

        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {
        }

        public DatabaseContext()
        {
            //Database.EnsureDeleted();
            //Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<Employee>()
                .HasOne<Role>(e => e.Role)
                .WithMany(r => r.Employees)
                .HasForeignKey(e => e.RoleId);

            builder.Entity<PromoCode>()
                .HasOne<Preference>(pr => pr.Preference)
                .WithMany(p => p.Promocodes)
                .HasForeignKey(bc => bc.PreferenceId);

            builder.Entity<PromoCode>()
                .HasOne<Employee>(pr => pr.PartnerManager)
                .WithMany(e => e.PromoCodes)
                .HasForeignKey(pr => pr.PartnerId);

            builder.Entity<CustomerPreference>().HasKey(cp => new { cp.CustomerId, cp.PreferenceId });

            builder.Entity<CustomerPreference>()
                .HasOne<Customer>(cp => cp.Customer)
                .WithMany(c => c.CustomerPreferences)
                .HasForeignKey(cp => cp.CustomerId);

            builder.Entity<CustomerPreference>()
                .HasOne<Preference>(cp => cp.Preference)
                .WithMany(c => c.CustomerPreferences)
                .HasForeignKey(cp => cp.PreferenceId);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Filename=localSqlLiteDb.db");
            optionsBuilder.EnableSensitiveDataLogging();
        }
    }
}