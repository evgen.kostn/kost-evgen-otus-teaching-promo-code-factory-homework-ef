﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T : BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }

        public void Dispose()
        { }

        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids)
        {
            return Task.FromResult(Data.Where(x => ids.Contains(x.Id)));
        }

        public Task<T> GetFirstWhere(Expression<Func<T, bool>> predicate)
        {
            //TODO
            return Task.FromResult(Data.FirstOrDefault());
        }

        public Task<IEnumerable<T>> GetWhere(Expression<Func<T, bool>> predicate)
        {
            //TODO
            return Task.FromResult(Data);
        }

        public Task<T> AddAsync(T item)
        {
            // TODO
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == item.Id));
        }

        public Task<T> UpdateAsync(T item)
        {
            // TODO
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == item.Id));
        }

        public Task DeleteAsync(Guid id)
        {
            // TODO
            return Task.FromResult(true);
        }

        public Task DeleteAsync(T item)
        {
            // TODO
            return Task.FromResult(true);
        }

        public Task SaveAsync(T item)
        {
            // TODO
            return Task.FromResult(true);
        }
    }
}