﻿using System.ComponentModel.DataAnnotations;
using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class GivePromoCodeRequest
    {
        [Required]
        [StringLength(50)]
        public string PromoCode { get; set; }

        [StringLength(500)]
        public string ServiceInfo { get; set; }

        [Required]
        [StringLength(200)]
        public string PartnerName { get; set; }

        [Required]
        [StringLength(500)]
        public string PreferenceName { get; set; }
    }
}