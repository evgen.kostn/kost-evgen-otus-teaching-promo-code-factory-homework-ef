﻿using System;
using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class PreferencePromocodeResponse
    {
        public List<Preference> Preferences { get; set; } = new List<Preference>();
        public List<PromoCode> Promocodes { get; set; } = new List<PromoCode>();
    }
}