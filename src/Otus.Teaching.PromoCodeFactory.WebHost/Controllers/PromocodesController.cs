﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IRepository<PromoCode> _promocodeRepository;
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<Customer> _customerRepository;

        public PromocodesController(IRepository<PromoCode> promocodeRepository,
                                    IRepository<Employee> employeeRepository,
                                    IRepository<Preference> preferenceRepository,
                                    IRepository<Customer> customerRepository)
        {
            _promocodeRepository = promocodeRepository;
            _employeeRepository = employeeRepository;
            _preferenceRepository = preferenceRepository;
            _customerRepository = customerRepository;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<PromoCodeShortResponse>> GetPromocodesAsync()
        {
            var promocodes = await _promocodeRepository.GetAllAsync();
            var response = promocodes.Select(x => ConvertToPromoCodeShortResponse(x)).ToList();

            return response;
        }

        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost("PromocodeForCustomers")]
        public async Task<ActionResult<PromoCodeResponse>> GivePromoCodesToCustomersWithPreferenceAsync([FromBody] GivePromoCodeRequest request)
        {
            // проверить данные
            var partner = await GetPartner(request.PartnerName);
            if ( partner == null )
                return BadRequest(request);

            var preference = await GetPreference(request.PreferenceName);
            if ( preference == null )
                return BadRequest(request);

            //Создать промокод
            var promocode = new PromoCode
            {
                Code = request.PromoCode,
                ServiceInformation = request.ServiceInfo,
                BeginDate = DateTime.Today,
                EndDate = DateTime.Today.AddDays(90),
                PartnerId = partner.Id,
                PartnerManager = partner,
                PreferenceId = preference.Id,
                Preference = preference
            };

            promocode = await _promocodeRepository.AddAsync(promocode);
            if ( promocode == null )
                return BadRequest(request);

            var result = new PromoCodeResponse
            {
                Id = promocode.Id,
                Code = promocode.Code,
                ServiceInfo = promocode.ServiceInformation,
                BeginDate = promocode.BeginDate.ToShortDateString(),
                EndDate = promocode.EndDate.ToShortDateString(),
                PartnerName = promocode.PartnerManager.FullName,
                PreferenceName = promocode.Preference.Name,
            };

            // и выдать его клиентам с указанным предпочтением:
            // это произойдет автоматически через связь с Preference

            // вернуть данные со списком клиентов
            if ( preference.CustomerPreferences != null && preference.CustomerPreferences.Count > 0 )
            {
                var custIds = preference.CustomerPreferences.Select(x => x.CustomerId).ToList();
                var customers = await _customerRepository.GetRangeByIdsAsync(custIds);

                result.Customers = (from cust in customers
                                    select new CustomerShortResponse
                                    {
                                        Id = cust.Id,
                                        FirstName = cust.FirstName,
                                        LastName = cust.LastName,
                                        Email = cust.Email,
                                    }).ToList();
            }

            return result;
        }

        /// <summary>
        /// Получить промокод по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<PromoCodeShortResponse>> GetPromocodeByIdAsync(Guid id)
        {
            var promocode = await _promocodeRepository.GetByIdAsync(id);

            if ( promocode == null )
                return NotFound();

            return ConvertToPromoCodeShortResponse(promocode);
        }

        /// <summary>
        /// Добавить промокод
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<PromoCodeShortResponse>> AddPromocodeAsync([FromBody] GivePromoCodeRequest request)
        {
            var partner = await GetPartner(request.PartnerName);
            if ( partner == null )
                return BadRequest(request);

            var preference = await GetPreference(request.PreferenceName);
            if ( preference == null )
                return BadRequest(request);

            var promocode = new PromoCode
            {
                Code = request.PromoCode,
                ServiceInformation = request.ServiceInfo,
                BeginDate = DateTime.Today,
                EndDate = DateTime.Today.AddDays(90),
                PartnerId = partner.Id,
                PartnerManager = partner,
                PreferenceId = preference.Id,
                Preference = preference,
            };

            promocode = await _promocodeRepository.AddAsync(promocode);
            if ( promocode == null )
                return BadRequest(request);

            return ConvertToPromoCodeShortResponse(promocode);
        }

        /// <summary>
        /// Удалить промокод по Id
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult> DeletePromocodeAsync(Guid id)
        {
            var role = await _promocodeRepository.GetByIdAsync(id);
            if ( role == null )
                return NotFound();

            await _promocodeRepository.DeleteAsync(id);

            return Ok();
        }

        /// <summary>
        /// Изменить данные промокода
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id:guid}")]
        public async Task<ActionResult<PromoCodeShortResponse>> UpdatePromocodeAsync(Guid id, [FromBody] GivePromoCodeRequest request)
        {
            var updPromoCode = await _promocodeRepository.GetByIdAsync(id);
            if ( updPromoCode == null )
                return NotFound();

            var partner = await GetPartner(request.PartnerName);
            if ( partner == null )
                return BadRequest(request);

            var preference = await GetPreference(request.PreferenceName);
            if ( preference == null )
                return BadRequest(request);

            updPromoCode.Code = request.PromoCode;
            updPromoCode.ServiceInformation = request.ServiceInfo;
            updPromoCode.PartnerId = partner.Id;
            updPromoCode.PartnerManager = partner;
            updPromoCode.PreferenceId = preference.Id;
            updPromoCode.Preference = preference;

            updPromoCode = await _promocodeRepository.UpdateAsync(updPromoCode);
            if ( updPromoCode == null )
                return BadRequest();

            return ConvertToPromoCodeShortResponse(updPromoCode);
        }

        private async Task<Preference> GetPreference(string name)
        {
            return await _preferenceRepository.GetFirstWhere(x => x.Name.Equals(name));
        }

        private async Task<Employee> GetPartner(string name)
        {
            Employee result = null;
            name = name.Trim();
            int ind = name.IndexOf(' ');
            if ( ind > 0 )
            {
                string firstName = name[..ind];
                string lastName = name[(ind + 1)..];
                result = await _employeeRepository.GetFirstWhere(x => x.FirstName.Equals(firstName) && x.LastName.Equals(lastName));
            }
            else
                result = await _employeeRepository.GetFirstWhere(x => x.FirstName.Equals(name));

            return result;
        }

        private static PromoCodeShortResponse ConvertToPromoCodeShortResponse(PromoCode promoCode)
        {
            var promoCodeModel = new PromoCodeShortResponse()
            {
                Id = promoCode.Id,
                Code = promoCode.Code,
                ServiceInfo = promoCode.ServiceInformation,
                BeginDate = promoCode.BeginDate.ToShortDateString(),
                EndDate = promoCode.EndDate.ToShortDateString(),
                PartnerName = promoCode.PartnerManager != null ? promoCode.PartnerManager.FullName : "",
                PreferenceName = promoCode.Preference != null ? promoCode.Preference.Name : ""
            };

            return promoCodeModel;
        }
    }
}