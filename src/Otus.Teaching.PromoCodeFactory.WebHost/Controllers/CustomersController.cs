﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<PromoCode> _promocodeRepository;

        public CustomersController(IRepository<Customer> customerRepository,
                                   IRepository<Preference> preferenseRepository,
                                   IRepository<PromoCode> promocodeRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenseRepository;
            _promocodeRepository = promocodeRepository;
        }

        /// <summary>
        /// Получить данные всех клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<CustomerShortResponse>> GetCustomersAsync()
        {
            var customers = await _customerRepository.GetAllAsync();

            var customersModelList = customers.Select(x =>
                new CustomerShortResponse()
                {
                    Id = x.Id,
                    Email = x.Email,
                    FirstName = x.FirstName,
                    LastName = x.LastName
                }).ToList();

            return customersModelList;
        }

        /// <summary>
        /// Получить данные клиента по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if ( customer == null )
                return NotFound();

            var prefIds = customer.CustomerPreferences.Select(x => x.PreferenceId).ToList();
            var preferencePromocodes = await GetPreferecesPromocodes(prefIds);
            customer.Preferences = preferencePromocodes.Preferences;
            customer.Promocodes = preferencePromocodes.Promocodes;

            return ConvertToCustomerResponse(customer);
        }

        /// <summary>
        /// Добавить нового клиента
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<CustomerResponse>> AddCustomerAsync([FromBody] CreateOrEditCustomerRequest request)
        {
            var customer = new Customer()
            {
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email
            };

            var preferencePromocodes = await GetPreferecesPromocodes(request.PreferenceIds);
            customer.Preferences = preferencePromocodes.Preferences;
            customer.Promocodes = preferencePromocodes.Promocodes;

            customer = await _customerRepository.AddAsync(customer);
            return ConvertToCustomerResponse(customer);
        }

        /// <summary>
        /// Изменить данные клиента
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id:guid}")]
        public async Task<ActionResult<CustomerResponse>> UpdateCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            Customer customer = await _customerRepository.GetByIdAsync(id);
            if ( customer == null )
                return NotFound(request);

            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.Email = request.Email;

            var preferencePromocodes = await GetPreferecesPromocodes(request.PreferenceIds);
            customer.Preferences = preferencePromocodes.Preferences;
            customer.Promocodes = preferencePromocodes.Promocodes;

            customer = await _customerRepository.UpdateAsync(customer);

            return ConvertToCustomerResponse(customer);
        }

        /// <summary>
        /// Удалить данные клиента по Id
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult> DeleteCustomer(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            if ( customer == null )
                return NotFound();

            var prefIds = customer.CustomerPreferences.Select(x => x.PreferenceId).ToList();
            var preferencePromocodes = await GetPreferecesPromocodes(prefIds);
            customer.Preferences = preferencePromocodes.Preferences;

            await _customerRepository.DeleteAsync(customer);

            return Ok();
        }

        private async Task<PreferencePromocodeResponse> GetPreferecesPromocodes(List<Guid> preferenceIds)
        {
            var preferences = await _preferenceRepository.GetRangeByIdsAsync(preferenceIds);
            var promoCodes = await _promocodeRepository.GetWhere(x => preferenceIds.Contains(x.PreferenceId));

            var result = new PreferencePromocodeResponse();
            if ( preferences != null )
                result.Preferences = preferences.ToList();

            if ( promoCodes != null )
                result.Promocodes = promoCodes.ToList();

            return result;
        }

        private static CustomerResponse ConvertToCustomerResponse(Customer customer)
        {
            var customerModel = new CustomerResponse()
            {
                Id = customer.Id,
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName
            };

            customerModel.Preferences = (from preference in customer.Preferences
                                         select new PreferenceResponse()
                                         {
                                             Id = preference.Id,
                                             Name = preference.Name
                                         }).ToList();

            customerModel.PromoCodes = (from promocode in customer.Promocodes
                                        select new PromoCodeShortResponse()
                                        {
                                            Id = promocode.Id,
                                            Code = promocode.Code,
                                            ServiceInfo = promocode.ServiceInformation,
                                            PartnerName = promocode.PartnerManager.FullName,
                                            PreferenceName = promocode.Preference.Name,
                                            BeginDate = promocode.BeginDate.ToShortDateString(),
                                            EndDate = promocode.EndDate.ToShortDateString()
                                        }).ToList();

            return customerModel;
        }
    }
}