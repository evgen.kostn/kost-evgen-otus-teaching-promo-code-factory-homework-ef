﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeesController(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x =>
                new EmployeeShortResponse()
                {
                    Id = x.Id,
                    Email = x.Email,
                    FullName = x.FullName,
                }).ToList();

            return employeesModelList;
        }

        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if ( employee == null )
                return NotFound();

            return ConvertToEmployeeResponse(employee);
        }

        /// <summary>
        /// Добавить нового сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<EmployeeResponse>> AddEmployeeAsync([FromBody] EmployeeShortRequest employeeData)
        {
            var employee = new Employee()
            {
                FirstName = employeeData.FirstName,
                LastName = employeeData.LastName,
                Email = employeeData.Email,
                AppliedPromocodesCount = employeeData.AppliedPromocodesCount
            };

            var new_employee = await _employeeRepository.AddAsync(employee);
            if ( new_employee == null )
                return BadRequest();

            return ConvertToEmployeeResponse(new_employee);
        }

        /// <summary>
        /// Удалить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult> DeleteEmployeeAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);
            if ( employee == null )
                return NotFound();

            await _employeeRepository.DeleteAsync(id);

            return Ok();
        }

        /// <summary>
        /// Изменить данные сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> UpdateEmployeeAsync(Guid id, [FromBody] EmployeeShortRequest employeeData)
        {
            var updEmployee = await _employeeRepository.GetByIdAsync(id);
            if ( updEmployee == null )
                return NotFound();

            updEmployee.FirstName = employeeData.FirstName;
            updEmployee.LastName = employeeData.LastName;
            updEmployee.Email = employeeData.Email;
            updEmployee.AppliedPromocodesCount = employeeData.AppliedPromocodesCount;
            updEmployee.Role = employeeData.Role;

            updEmployee = await _employeeRepository.UpdateAsync(updEmployee);

            if ( updEmployee == null )
                return BadRequest();

            return ConvertToEmployeeResponse(updEmployee);
        }

        private static EmployeeResponse ConvertToEmployeeResponse(Employee employee)
        {
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Role = new RoleItemResponse()
                {
                    Id = employee.Role.Id,
                    Name = employee.Role.Name,
                    Description = employee.Role.Description
                },
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }
    }
}