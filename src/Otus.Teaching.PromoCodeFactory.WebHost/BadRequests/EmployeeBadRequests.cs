﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using System;
using System.Data;
using System.Diagnostics;
using System.Net.Http;
using System.Net;
using System.Runtime.CompilerServices;
using System.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Routing;

namespace Otus.Teaching.PromoCodeFactory.WebHost.BadRequests
{
    internal class EmployeeBadRequests
    {
        private static BadRequestObjectResult EmployeeException(string exceptionText)
        {
            return new BadRequestObjectResult(exceptionText);
        }

        public static BadRequestObjectResult EmployeeExistsError(string email)
        {
            return EmployeeException($"Сотрудник с почтой {email} уже существует!");
        }

        public static BadRequestObjectResult EmployeeMailFormatError(string email)
        {
            return EmployeeException($"Почта {email} имеет некорректный формат!");
        }

        public static BadRequestObjectResult EmployeeNotExistsError(string email)
        {
            return EmployeeException($"Сотрудник с почтой {email} не существует!");
        }

        public static BadRequestObjectResult EmployeeRoleNotExistsError(string role)
        {
            return EmployeeException($"Роль {role} не существует!");
        }

        public static BadRequestObjectResult EmployeeGuidFormatError(string guid)
        {
            return EmployeeException($"Некорректный формат ID {guid}!");
        }

        public static BadRequestObjectResult EmployeeGuidNotExistsError(string guid)
        {
            return EmployeeException($"Пользователя с ID {guid} не существует!");
        }
    }
}
